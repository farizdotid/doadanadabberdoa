package com.nurfalah.doadanadabberdoa.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nurfalah.doadanadabberdoa.R;
import com.nurfalah.doadanadabberdoa.adapter.PencarianDoaAdapterRemastered;
import com.nurfalah.doadanadabberdoa.model.DaftarDoa;
import com.nurfalah.doadanadabberdoa.model.ResponseDaftarDoa;
import com.nurfalah.doadanadabberdoa.util.Helper;
import com.nurfalah.doadanadabberdoa.util.RecyclerItemClickListener;
import com.nurfalah.doadanadabberdoa.util.apiservice.BaseApiService;
import com.nurfalah.doadanadabberdoa.util.apiservice.UtilsApi;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PencarianActivity extends AppCompatActivity {

    private static final String TAG = "debug";

    @BindView(R.id.etCariDoa)
    EditText etCariDoa;
    @BindView(R.id.rvCariDoa)
    RecyclerView rvCariDoa;
    @BindView(R.id.btnCariDoa)
    Button btnCariDoa;
    ProgressDialog loading;

    Context mContext;
    PencarianDoaAdapterRemastered mPencarianAdapter;
    List<DaftarDoa> daftarDoaList = new ArrayList<>();

    BaseApiService mApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pencarian);
        getSupportActionBar().setTitle("Cari Doa");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ButterKnife.bind(this);
        mContext = this;
        mPencarianAdapter = new PencarianDoaAdapterRemastered(daftarDoaList, mContext);
        mApiService = UtilsApi.getAPIService();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvCariDoa.setLayoutManager(mLayoutManager);
        rvCariDoa.setItemAnimator(new DefaultItemAnimator());
        rvCariDoa.setAdapter(mPencarianAdapter);

        btnCariDoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Helper.checkInternet(mContext)){
                    loading = ProgressDialog.show(mContext, null, "Sedang Mencari Doa...", true, false);
                    getSearchDoa(etCariDoa.getText().toString());
                } else {
                    Toast.makeText(mContext, "Tidak Ada Koneksi Internet", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getSearchDoa(String keyword){
        mApiService.getSearchDoa(keyword).enqueue(new Callback<ResponseDaftarDoa>() {
            @Override
            public void onResponse(Call<ResponseDaftarDoa> call, Response<ResponseDaftarDoa> response) {
                if (response.isSuccessful()){
                    loading.dismiss();
                    if (response.body().getSuccess() == 1) {
                        final List<DaftarDoa> daftarDoaItems = response.body().getDaftarDoa();
                        rvCariDoa.setAdapter(new PencarianDoaAdapterRemastered(daftarDoaItems, mContext));
                        mPencarianAdapter.notifyDataSetChanged();

                        rvCariDoa.addOnItemTouchListener(
                                new RecyclerItemClickListener(mContext, new RecyclerItemClickListener.OnItemClickListener() {
                                    @Override public void onItemClick(View view, int position) {
                                        // TODO Handle item click
                                        String namadoa = daftarDoaItems.get(position).getNamaDoa();
                                        String bacaanarab = daftarDoaItems.get(position).getBacaanArab();
                                        String bacaanlatin = daftarDoaItems.get(position).getBacaanLatin();
                                        String artidoa = daftarDoaItems.get(position).getArtiDoa();
                                        String sumberdoa = daftarDoaItems.get(position).getSumberDoa();
                                        String isisumber = daftarDoaItems.get(position).getIsiSumber();

                                        Intent detailIntent = new Intent(mContext, DetailDoaActivity.class);
                                        detailIntent.putExtra("namadoa", namadoa);
                                        detailIntent.putExtra("bacaanarab", bacaanarab);
                                        detailIntent.putExtra("bacaanlatin", bacaanlatin);
                                        detailIntent.putExtra("artidoa", artidoa);
                                        detailIntent.putExtra("sumberdoa", sumberdoa);
                                        detailIntent.putExtra("isisumber", isisumber);
                                        startActivity(detailIntent);
                                    }
                                })
                        );
                    } else {
                        Toast.makeText(mContext, "Doa Tidak Ditemukan", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d(TAG, "onResponse: GA BERHASIL");
                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseDaftarDoa> call, Throwable t) {
                Log.e(TAG, "onFailure: ERROR "+ t.getMessage());
                loading.dismiss();
                Toast.makeText(mContext, "Koneksi Internet Bermasalah", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
