package com.nurfalah.doadanadabberdoa.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.nurfalah.doadanadabberdoa.R;
import com.nurfalah.doadanadabberdoa.adapter.DaftarDoaAdapter;
import com.nurfalah.doadanadabberdoa.model.DaftarDoa;
import com.nurfalah.doadanadabberdoa.model.ResponseDaftarDoa;
import com.nurfalah.doadanadabberdoa.util.Helper;
import com.nurfalah.doadanadabberdoa.util.RecyclerItemClickListener;
import com.nurfalah.doadanadabberdoa.util.apiservice.BaseApiService;
import com.nurfalah.doadanadabberdoa.util.apiservice.UtilsApi;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeScreenActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "debug";

    @BindView(R.id.rvDaftarDoa)
    RecyclerView rvDaftarDoa;
    ProgressDialog loading;
    Context mContext;

    List<DaftarDoa> daftarDoaList;
    DaftarDoaAdapter daftarDoaAdapter;

    BaseApiService mApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);
        mApiService = UtilsApi.getAPIService();
        daftarDoaAdapter = new DaftarDoaAdapter(this, daftarDoaList);
        mContext = this;

        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_search_black_24dp);
        DrawableCompat.setTint(drawable, ContextCompat.getColor(this, R.color.white));
        toolbar.setOverflowIcon(drawable);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvDaftarDoa.setLayoutManager(mLayoutManager);
        rvDaftarDoa.setItemAnimator(new DefaultItemAnimator());

        if (Helper.checkInternet(this)){
            loading = ProgressDialog.show(mContext, null, "Harap Tunggu...", true, false);
            getData();
        } else {
            Toast.makeText(mContext, "Tidak Ada Koneksi Internet", Toast.LENGTH_SHORT).show();
        }
    }

    private void getData(){
        mApiService.getAllDoa().enqueue(new Callback<ResponseDaftarDoa>() {
            @Override
            public void onResponse(Call<ResponseDaftarDoa> call, Response<ResponseDaftarDoa> response) {
                if (response.isSuccessful()){
                    Log.d(TAG, "onResponse: BERHASIL");
                    loading.dismiss();
                    final List<DaftarDoa> daftarDoaItems = response.body().getDaftarDoa();
                    rvDaftarDoa.setAdapter(new DaftarDoaAdapter(mContext, daftarDoaItems));
                    daftarDoaAdapter.notifyDataSetChanged();

                    rvDaftarDoa.addOnItemTouchListener(
                            new RecyclerItemClickListener(mContext, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override public void onItemClick(View view, int position) {
                                    // TODO Handle item click
                                    String namadoa = daftarDoaItems.get(position).getNamaDoa();
                                    String bacaanarab = daftarDoaItems.get(position).getBacaanArab();
                                    String bacaanlatin = daftarDoaItems.get(position).getBacaanLatin();
                                    String artidoa = daftarDoaItems.get(position).getArtiDoa();
                                    String sumberdoa = daftarDoaItems.get(position).getSumberDoa();
                                    String isisumber = daftarDoaItems.get(position).getIsiSumber();
                                    String artisumber = daftarDoaItems.get(position).getArti_sumber();
                                    String gambar = daftarDoaItems.get(position).getGambar();

                                    Intent detailIntent = new Intent(mContext, DetailDoaActivity.class);
                                    detailIntent.putExtra("namadoa", namadoa);
                                    detailIntent.putExtra("bacaanarab", bacaanarab);
                                    detailIntent.putExtra("bacaanlatin", bacaanlatin);
                                    detailIntent.putExtra("artidoa", artidoa);
                                    detailIntent.putExtra("sumberdoa", sumberdoa);
                                    detailIntent.putExtra("isisumber", isisumber);
                                    detailIntent.putExtra("artisumber", artisumber);
                                    detailIntent.putExtra("gambar", gambar);
                                    startActivity(detailIntent);
                                }
                            })
                    );
                } else {
                    Log.d(TAG, "onResponse: GA BERHASIL");
                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseDaftarDoa> call, Throwable t) {
                Log.e(TAG, "onFailure: GAGAGL");
                loading.dismiss();
                Toast.makeText(mContext, "Koneksi Internet Bermasalah", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_caridoa) {
            startActivity(new Intent(mContext, PencarianActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_qa) {
            startActivity(new Intent(mContext, QAActivity.class));
        } else if (id == R.id.nav_index) {
            startActivity(new Intent(mContext, IndexActivity.class));
        } else if (id == R.id.nav_tentang) {
            startActivity(new Intent(mContext, TentangActivity.class));
        } else if (id == R.id.nav_keluar) {
            showDialog();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Anda Yakin Ingin Keluar")
                .setCancelable(false)
                .setPositiveButton("Ya",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // jika tombol diklik, maka akan menutup activity ini
                        System.exit(0);
                    }
                })
                .setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // jika tombol ini diklik, akan menutup dialog
                        // dan tidak terjadi apa2
                        dialog.cancel();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }
}
