package com.nurfalah.doadanadabberdoa.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nurfalah.doadanadabberdoa.R;
import com.nurfalah.doadanadabberdoa.util.apiservice.UtilsApi;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailDoaActivity extends AppCompatActivity {

    @BindView(R.id.tvResultNamaDoa)
    TextView tvResultNamaDoa;
    @BindView(R.id.tvResultBacaanArab)
    TextView tvResultBacaanDoa;
    @BindView(R.id.tvResultBacaanLatin)
    TextView tvResultBacaanLatin;
    @BindView(R.id.tvResultArtiDoa)
    TextView tvResultArtiDoa;
    @BindView(R.id.tvResultSumberDoa)
    TextView tvResultSumberDoa;
    @BindView(R.id.tvResultIsiSumber)
    TextView tvResultIsiSumber;
    @BindView(R.id.tvResultArtiSumber)
    TextView tvResultArtiSumber;
    @BindView(R.id.ivResultGambar)
    ImageView ivResultGambar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_doa);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ButterKnife.bind(this);

        initResult();
    }

    private void initResult(){
        Intent intent = getIntent();
        String namadoa = intent.getStringExtra("namadoa");
        String bacaanarab = intent.getStringExtra("bacaanarab");
        String bacaanlatin = intent.getStringExtra("bacaanlatin");
        String artidoa = intent.getStringExtra("artidoa");
        String sumberdoa = intent.getStringExtra("sumberdoa");
        String isisumber = intent.getStringExtra("isisumber");
        String artisumber = intent.getStringExtra("artisumber");
        String gambar = intent.getStringExtra("gambar");

        tvResultNamaDoa.setText(namadoa);
        tvResultBacaanDoa.setText(bacaanarab);
        tvResultBacaanLatin.setText(bacaanlatin);
        tvResultArtiDoa.setText(artidoa);
        tvResultSumberDoa.setText(sumberdoa);
        tvResultIsiSumber.setText(isisumber);
        tvResultArtiSumber.setText(artisumber);
        Glide.with(this).load(UtilsApi.BASE_URL_API+"assets/img/gambar/"+gambar).into(ivResultGambar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
