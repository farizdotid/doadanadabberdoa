package com.nurfalah.doadanadabberdoa.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */


public class DaftarDoa {

    @SerializedName("no_doa")
    @Expose
    private String noDoa;
    @SerializedName("nama_doa")
    @Expose
    private String namaDoa;
    @SerializedName("bacaan_arab")
    @Expose
    private String bacaanArab;
    @SerializedName("bacaan_latin")
    @Expose
    private String bacaanLatin;
    @SerializedName("arti_doa")
    @Expose
    private String artiDoa;
    @SerializedName("sumber_doa")
    @Expose
    private String sumberDoa;
    @SerializedName("isi_sumber")
    @Expose
    private String isiSumber;
    @SerializedName("arti_sumber")
    @Expose
    private String arti_sumber;
    @SerializedName("gambar")
    @Expose
    private String gambar;

    public String getNoDoa() {
        return noDoa;
    }

    public void setNoDoa(String noDoa) {
        this.noDoa = noDoa;
    }

    public String getNamaDoa() {
        return namaDoa;
    }

    public void setNamaDoa(String namaDoa) {
        this.namaDoa = namaDoa;
    }

    public String getBacaanArab() {
        return bacaanArab;
    }

    public void setBacaanArab(String bacaanArab) {
        this.bacaanArab = bacaanArab;
    }

    public String getBacaanLatin() {
        return bacaanLatin;
    }

    public void setBacaanLatin(String bacaanLatin) {
        this.bacaanLatin = bacaanLatin;
    }

    public String getArtiDoa() {
        return artiDoa;
    }

    public void setArtiDoa(String artiDoa) {
        this.artiDoa = artiDoa;
    }

    public String getSumberDoa() {
        return sumberDoa;
    }

    public void setSumberDoa(String sumberDoa) {
        this.sumberDoa = sumberDoa;
    }

    public String getIsiSumber() {
        return isiSumber;
    }

    public void setIsiSumber(String isiSumber) {
        this.isiSumber = isiSumber;
    }

    public String getArti_sumber() {
        return arti_sumber;
    }

    public void setArti_sumber(String arti_sumber) {
        this.arti_sumber = arti_sumber;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }
}
