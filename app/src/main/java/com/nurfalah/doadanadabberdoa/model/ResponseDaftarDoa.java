package com.nurfalah.doadanadabberdoa.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */


public class ResponseDaftarDoa {

    @SerializedName("daftar_doa")
    @Expose
    private List<DaftarDoa> daftarDoa = null;
    @SerializedName("success")
    @Expose
    private Integer success;

    public List<DaftarDoa> getDaftarDoa() {
        return daftarDoa;
    }

    public void setDaftarDoa(List<DaftarDoa> daftarDoa) {
        this.daftarDoa = daftarDoa;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }
}
