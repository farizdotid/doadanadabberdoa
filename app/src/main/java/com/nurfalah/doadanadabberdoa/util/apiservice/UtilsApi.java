package com.nurfalah.doadanadabberdoa.util.apiservice;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */


public class UtilsApi {

//    public static final String BASE_URL_API = "http://dev.farizdotid.com/web/";
    public static final String BASE_URL_API = "http://dev.farizdotid.com/doa_arti/";

    public static BaseApiService getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }
}
