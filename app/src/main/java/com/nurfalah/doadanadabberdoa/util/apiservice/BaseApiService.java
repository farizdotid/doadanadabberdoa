package com.nurfalah.doadanadabberdoa.util.apiservice;

import com.nurfalah.doadanadabberdoa.model.ResponseDaftarDoa;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */


public interface BaseApiService {

    @GET("api/get_all_data_daftardoa.php")
    Call<ResponseDaftarDoa> getAllDoa();

    @GET("api/get_nama_doa.php?")
    Call<ResponseDaftarDoa> getSearchDoa(@Query("nama_doa") String namadoa);
}
