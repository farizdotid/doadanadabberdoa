package com.nurfalah.doadanadabberdoa.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nurfalah.doadanadabberdoa.R;
import com.nurfalah.doadanadabberdoa.model.DaftarDoa;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 * linkedin : https://www.linkedin.com/in/farizramadhan/
 */


public class DaftarDoaAdapter extends RecyclerView.Adapter<DaftarDoaAdapter.MyViewHolder> {

    Context mContext;
    List<DaftarDoa> daftarDoaList;

    public DaftarDoaAdapter(Context mContext, List<DaftarDoa> daftarDoaList) {
        this.mContext = mContext;
        this.daftarDoaList = daftarDoaList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_daftardoa, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DaftarDoa daftarDoa = daftarDoaList.get(position);
        holder.tvResultNamaDoa.setText(daftarDoa.getNamaDoa());
    }

    @Override
    public int getItemCount() {
        return daftarDoaList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvResultNamaDoa)
        TextView tvResultNamaDoa;

        public MyViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
